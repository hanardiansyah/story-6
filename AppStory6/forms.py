from django import forms
from .models import Status


class StatusForms(forms.ModelForm):
    class Meta:
        model = Status
        fields = ['status']
        widgets = {
        'status' : forms.Textarea(attrs={'cols':10, 'rows': 5, "class":"form-control"}),
        }



