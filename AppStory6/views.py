from django.shortcuts import render,redirect
from . import forms
from .models import Status

def index(request):
    if request.method == "POST":
        form = forms.StatusForms(request.POST)
        if form.is_valid():
            form.save()
            return redirect('index')
    else:
        form = forms.StatusForms()
    status = Status.objects.all().order_by('date').reverse()
    return render (request, 'index.html', {'form' : form, 'status' : status})
